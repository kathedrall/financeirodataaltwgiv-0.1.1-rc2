<?php

    namespace Sistema\Libs\Rest\Cliente;
    
    use Sistema\Libs\Rest;
    use Sistema\Libs\Processos\Autenticacao\Autenticacao;
    use Sistema\Libs\Rest\Faturamentos;  
    use Sistema\Libs\Fs\Io\Logs;

    class Faturamento implements Faturamentos
    {
        
        private $header;
        private $body;
        private $query;
        private $resposta;
        private $productId;
        private $msisdn;
        private $autenticacao;
        private $validacao;

        public function __construct(string $productId,
            string $msisdn
        ){
            $this->productId = $productId;
            $this->msisdn = $msisdn;
        }

        public function invoca()
        {
            try { 
                
                $this->autenticacao = new Autenticacao();          
                $this->header = array('Content-Type' => 'application/json',
                        "apiKey" => self::APIKEY,
                        "authentication" =>  $this->autenticacao->autenticacao());
           
                $this->body = array('productId' => $this->productId
                        , 'countryId ' => self::COUNTRY_ID
                        , 'pricepointId' => self::PRICE_POINT_ID
                        , 'mcc' => self::MCC
                        , 'mnc' => self::MNC
                        , 'msisdn' => $this->msisdn
                        , 'partnerId' => self::PARTNER_ID
                        , 'shortCode' => self::SHORT_CODE
                        , 'transactionType' => self::TRANSACTION_TYPE
                        , 'billingContextId' => self::BILLING_CONTEXT_ID
                        , 'channel' => self::CHANNELL);
                      
                $this->query =  \Unirest\Request\Body::json($this->body);
                $this->resposta = \Unirest\Request::post(self::URL,$this->header,$this->query);
               
                $this->resposta->code;        // HTTP Status code
                $this->resposta->headers;     // Headers
                $this->resposta->body;        // Parsed body
                $this->resposta->raw_body;    // Unparsed body

                $this->validacao = json_decode($this->resposta->raw_body, true);
            
            } catch(\Error  $error ){
               
                $this->log = new Logs();
                $this->log->error(__FUNCTION__, $error, $codigo );
                   
            }
              
            return $this->validacao;
        }

    
    }