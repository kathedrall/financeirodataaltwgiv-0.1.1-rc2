<?php

    namespace Sistema\Libs\Processos\Millis;

    abstract class Atual
    {
        public static function tempo() : string 
        {
            list($usec, $sec) = explode(" ", microtime());
            return round(((float) $usec + (float) $sec) * 1000); 
        }
    }