<?php

namespace Sistema\Libs\Processos\Manager;

class Pid implements \Countable
{
	protected $processes = array();
	protected $is_child  = FALSE;

	public function count()
	{
		return count($this->processes);
	}

	public function signal_handler($signal)
	{
		if ($this->is_child){
			return;
		}

		switch ($signal){
			case SIGINT:
			case SIGTERM:
				$this->kill_all();
				exit(0);

			case SIGCHLD:
				$this->reap_child();
		}
	}

	public function kill_all()
	{
		foreach ($this->processes as $pid => $is_running){
			posix_kill($pid, SIGKILL);
		}
	}

	public function fork_child($callback, $data)
	{
		$pid = pcntl_fork();

		switch($pid){
			case 0:
				$this->is_child = TRUE;
				call_user_func_array($callback, $data);
				posix_kill(posix_getppid(), SIGCHLD);
				exit;

			case -1:
				throw new Exception("Out of memory!");

			default:
				$this->processes[$pid] = TRUE;
				return $pid;
		}
	}

	public function reap_child()
	{
		$pid = pcntl_wait($status,  WNOHANG);
			if ($pid < 0){
				throw new Exception("Out of memory");
			} elseif ($pid > 0) {
				unset($this->processes[$pid]);
			}	
	}
}