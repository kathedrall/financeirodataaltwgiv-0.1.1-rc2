<?php

    namespace Sistema\Libs\Processos\Autenticacao;

    use Sistema\Libs\Rest\Faturamentos;
    use Sistema\Libs\Processos\Millis\Atual;
    use Sistema\Libs\Fs\Io\Logs;


    class Autenticacao implements Faturamentos
    {
        private $currentMillis;

        public function autenticacao() : string
        {
            try {
                $this->currentMillis = self::ROLEID . Atual::tempo();    
                return openssl_encrypt($this->currentMillis, self::TYPE, self::PASSWORD, 0, "");
            } catch(\Error $e) {

                $this->log = new Logs();
                $this->log->error(__FUNCTION__, "Error 003 " . $error->getMessage() . " - " . $error->getLine() . " - " . $error->getFile() . " - " . $error->getTrace(), $codigo );
          
            }
            
        }
    }