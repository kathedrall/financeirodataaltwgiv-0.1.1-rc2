<?php

namespace Sistema\Libs\Persistencia;

/**
 * Description of Entidade
 *
 * @author kathedrall
 */
class Entidade
{
    private $a_param;

    public function __get($key) 
    {
        return $this->a_param[$key];
    }

    public function __set($key, $value) 
    {
        $this->a_param[$key] = $value;
    }
}
