<?php

    namespace Sistema\Libs\Fs\Io;

    use Sistema\Libs\Fs\Io\Io;

    class Arquivo extends Io 
    {
        const ARQUIVO_PROCESSADO = 'jokes.txt';
        private $listar;
        private $arquivo;
        private $fila;
        private $quantidade;
        private $dado;
        
     
        public function ler()
        {
            
            define("REPOSITORIO",dirname(__DIR__,5) . '/repositorio');                
            $this->listar = $this->listar(REPOSITORIO);
            
                if(NULL !== $this->listar){                  
                    $this->arquivo = self::sorteio($this->listar);
                    $c = REPOSITORIO.'/'.$this->arquivo;
                    $this->fila = $this->abrir($c);
                    $this->dado = $this->quebra($this->fila);
                    if($this->fila->last()){
                      self::excluir($c);        
                    }
                } else $this->dado = NULL;
                
            return $this->dado;
        }

        public function processado(int $codigo, 
            string $parametro
        ) : void 
        {
            define("URL", dirname(__DIR__,5) . '/processado');
            $concatenar = $codigo .",".$parametro."|";

            $linha = $concatenar."\r\n";
            chdir(URL);
            $fopen = fopen(self::ARQUIVO_PROCESSADO, "a+");
            fwrite($fopen, $linha);
            fclose($fopen);


        }
        private function quebra(\Ds\Deque $fila) : \Ds\Vector
        {
            $this->quantidade = count($fila);
          
            $container = new \Ds\Vector();

                for($i=0; $i < $this->quantidade; $i++){
                    list($msisdn, $productId, $id) = explode('|', $fila->get($i));
                    $container->push(new \Ds\Vector([trim($msisdn),trim($productId),trim($id)]));
                    
                }
                    
            return $container;
        }
        private static function sorteio(array $conteudo) : string
        {
            return $conteudo[mt_rand(2, count($conteudo) -1)];
        }
    
        
        
    }
