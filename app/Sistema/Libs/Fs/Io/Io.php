<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Sistema\Libs\Fs\Io;

use Sistema\Libs\Persistencia\Entidade;

class Io
{
    
   protected $diretorio;
   protected $ler;
   private $dados; 
   private $scaner;
   
    /**
     * lista arquivos que estao armazenados no diretorio repositorio
     * @access Protected
     * @return array
     */
    protected function listar(string $caminho) 
    {
        $this->scaner = scandir($caminho ,1);
        array_pop($this->scaner);
        array_pop($this->scaner);
       
         return count($this->scaner) >= 1 ? $this->scaner : NULL;
       
    }

    protected function abrir(string $arquivo) : \Ds\Deque
    {
        $fila = new \Ds\Deque();
            $this->ler = fopen($arquivo, 'r+');
                while(!feof($this->ler)){
                    $this->dados = fgets($this->ler, 8024);
                    $fila->push(trim($this->dados));
                }
            fclose($this->ler);
        $fila->pop();
        //$fila->push(NULL);
        return $fila;        
            
    }

    protected static function excluir(string $nomeArquivo) : void
    {
        unlink($nomeArquivo);
    }
}
