<?php

    namespace Sistema\Libs\Fs\Io;

    class Logs
    {
        private $data;
        private $hora;
        private $arquivo;
        private $linha;
        private $fopen;

        private static function caminho(bool $ponteiro) : string 
        {
            $a = (TRUE == $ponteiro) ? '/logs/' : '/logs/erros/';
            return dirname(__DIR__, 5) . $a;
        }

        public function logs(int $codigo,
            string $msisdn,
            string $parametro
        ) : void {
            date_default_timezone_set("America/Sao_Paulo");

            $this->data = date('y-m-d');
	        $this->hora = date('H:i:s');
            $this->arquivo = "LoggerDOB-".$this->data.".log";
            $this->linha = $codigo .";". $msisdn . ";". $this->data.'-'.$this->hora . ";" . $parametro ." \r\n ";
        
            chdir(self::caminho(TRUE));
      
            $this->fopen = fopen($this->arquivo, "a+");
            fwrite($this->fopen, $this->linha);
            fclose($this->fopen);
            
        }

        public function error(string $metodo,
            string $messagem, 
            string $msisdn = NULL
        ) : void {
            
            date_default_timezone_set("America/Sao_Paulo");
            $this->data = date('y-m-d');
            $this->hora = date('H:i:s');
            $this->arquivo = "Logger".$metodo."-".$this->data.".log";
            $this->linha = $metodo . ";" . $messagem ." \r\n ";
            
            chdir(self::caminho(FALSE));
    
            $this->fopen = fopen($this->arquivo, "a+");
            fwrite($this->fopen, $this->linha);
            fclose($this->fopen);
             
        }
    }
