<?php

    use Sistema\Libs\Processos\Manager\Pid;
    use Sistema\Libs\Fs\Io\Arquivo;
    use Sistema\Libs\Rest\Cliente\Faturamento;
    use Sistema\DAO\Faturamento\Recebimento;
    use Sistema\Libs\Fs\Io\Logs;
    
    require_once 'app/start.php';
    
    class Main
    {
       
        private $carga;
        private $log;
        private $cliente;
        private $retorno;
        private $lista;

        public function carga() 
        {
            $this->carga = new Arquivo();
            $this->lista = $this->carga->ler();
           
              if(NULL !== $this->lista)
                for($i=0;$i < count($this->lista); $i++){                
                  $fila[] = array(
                            'msisdn' => $this->lista->get($i)->get(0),
                            'productId' => $this->lista->get($i)->get(1),    
                            'codigo' => $this->lista->get($i)->get(2),                   
                          );
                } else $fila = NULL;
            
            return $fila;
        }

        public function processarCarga() : void
        {
            $pm = new Pid();
          
            declare(ticks = 1);
          
            $callback = array($pm, 'signal_handler');
            pcntl_signal(SIGTERM, $callback);
            pcntl_signal(SIGINT,  $callback);
            pcntl_signal(SIGCHLD, $callback);

            
              $sender = function($message_id, $message){

                $ms = rand(500, 1000);
                usleep($ms * 1000);
       
                $this->cliente = new Faturamento($message['productId'], $message['msisdn']);
                $this->retorno = $this->cliente->invoca();     
            
                self::registroNovoFaturamento(intval($message['codigo']),$this->retorno['code']);
         

                $this->log = new Logs();
                $this->log->logs($message['msisdn'],$this->retorno['code'], $message['codigo']);
              };
                $start_time = microtime(TRUE);

                  $carga = $this->carga();  
                      
                    if(NULL !== $carga){
                        foreach ($carga as $message_id => $message){
                            $args = array(
                                'message_id' => $message_id,
                                'message' => $message,                    
                            );

                          $pm->fork_child($sender, $args);                      
                          if (count($pm) >= 5) while (count($pm) >= 5) usleep(500000); // sleep 500 ms
                        }
                      
                      while (count($pm) > 0) usleep(500000); // sleep 500 ms
                      $runtime = microtime(TRUE) - $start_time;
                      
                  }
                  
                exit;  
        }

        public function inicio()
        {
          date_default_timezone_set("America/Sao_Paulo");
          
          $h = array("18:58","11:58");          
          $horaUm    = strtotime($h[0]);
          $horaDois  = strtotime($h[1]);
          $horaAtual = strtotime('now');
              
          switch ($horaAtual)
          {
              case ($horaAtual > $horaUm && $horaAtual < $horaDois) : 

                $this->log = new Logs();
                $this->log->error(__FUNCTION__,' pausa  - HORA UM'. date('h:i a', strtotime($h[0])).' - hora dois '. date('h:i a', strtotime($h[1])) .' - '. date('h:i a', strtotime('now')) , NULL );
                break; 
              default :  
                $this->log = new Logs();
                $this->log->error(__FUNCTION__,' processando  - HORA UM'. date('h:i a', strtotime($h[0])).' - hora dois '. date('h:i a', strtotime($h[1])) .' - '. date('h:i a', strtotime('now')) , NULL );
                $this->processarCarga();
               
          }

        }

        private static function registroNovoFaturamento(int $codigo,
          string $parametro
        ): void 
        {
          
            $processado = new Arquivo();
            $processado->processado($codigo,$parametro);
        }

    }

    $w = new EvPeriodic(0., 720, NULL, function ($w, $revents) 
    {
      $programa = new Main();
      $programa->inicio();
    });  
  Ev::run();  
